﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CalculatorLog
{
    class clsCalculationEngine
    {
        
        public double TriCalculationEngine(string strOpearation, double u64CalNumber)
        {
            double u64Result = 0;
            string DisplayData = "";

            switch (strOpearation)
            {
                case "sin":
                    u64Result = Math.Sin(u64CalNumber);
                    return u64Result;
                    break;

                case "cos":
                    u64Result = Math.Cos(u64CalNumber);
                   return u64Result;
                    break;

                case "tan":
                    u64Result = Math.Tan(u64CalNumber);
                    return u64Result;
                    break;

                case "csc":

                    if (u64CalNumber == 0)
                    {
                        MessageBox.Show("Can not divide by zero");
                    }
                    else
                    {
                        u64Result = Math.Pow(Math.Sin(u64CalNumber), -1);
                        return u64Result;
                    }
                    break;

                case "sec":
                    u64Result = Math.Pow(Math.Cos(u64CalNumber), -1);
                    return u64Result;
                    break;

                case "cot":
                    if (u64CalNumber == 0)
                    {
                        MessageBox.Show("Can not divide by zero");
                    }
                    else
                    {
                        u64Result = Math.Pow(Math.Tan(u64CalNumber), -1);
                        return u64Result;
                    }
                    break;

                default:
                    break;
                    
            }
            return u64Result;
        }

        public double TriArithmaticOperation(string strOpearation, double u64Result, double u64SecondNumber)
        {
            switch (strOpearation)
            {
                case "+":
                    u64Result = u64Result + u64SecondNumber;
                    return u64Result;
                    break;

                case "-":
                    u64Result = u64Result - u64SecondNumber;
                    return u64Result;
                    break;

                case "/":
                    u64Result = u64Result / u64SecondNumber;
                    return u64Result;
                    break;

                default:
                    break;

            }
            return u64Result;
        }
        public double evaluate(string expression)
        {
            char[] chrArrayPayloadtokens = expression.ToCharArray();

            Stack<double> u64Payloadvalues = new Stack<double>();
            Stack<char> chrOperatins = new Stack<char>();

            for (int i = 0; i < chrArrayPayloadtokens.Length; i++)
            {


                if (chrArrayPayloadtokens[i] == ' ')
                {

                    continue;
                }
                if (chrArrayPayloadtokens[i] >= '0' && chrArrayPayloadtokens[i] <= '9')
                {
                    StringBuilder sbuf = new StringBuilder();
                    while (i < chrArrayPayloadtokens.Length && chrArrayPayloadtokens[i] >= '0' && chrArrayPayloadtokens[i] <= '9')
                    {
                        sbuf.Append(chrArrayPayloadtokens[i++]);
                    }

                    u64Payloadvalues.Push(double.Parse(sbuf.ToString()));
                }
                else if (chrArrayPayloadtokens[i] == '+' || chrArrayPayloadtokens[i] == '-' || chrArrayPayloadtokens[i] == '/')
                {
                    while (chrOperatins.Count > 0 && hasPrecedence(chrArrayPayloadtokens[i], chrOperatins.Peek()))
                    {
                        u64Payloadvalues.Push(applyOp(chrOperatins.Pop(), u64Payloadvalues.Pop(), u64Payloadvalues.Pop()));
                    }
                    chrOperatins.Push(chrArrayPayloadtokens[i]);
                }
            }

            while (chrOperatins.Count > 0)
            {
                u64Payloadvalues.Push(applyOp(chrOperatins.Pop(), u64Payloadvalues.Pop(), u64Payloadvalues.Pop()));
            }



            return u64Payloadvalues.Pop();

        }

        public static bool hasPrecedence(char chrOperand1, char chrOperand2)
        {
            if ((chrOperand1 == '/') && (chrOperand2 == '+' || chrOperand2 == '-'))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public double applyOp(char chrOperation, double u64Value1, double u64Value2)
        {
            switch (chrOperation)
            {
                case '+':
                    double data = 0.0;
                    data = u64Value1 + u64Value2;
                    return data;
                case '-':
                    return u64Value2 - u64Value1;
                case '*':
                    return u64Value1 * u64Value2;
                case '/':
                    if (u64Value2 == 0)
                    {
                        throw new System.NotSupportedException("Cannot divide by zero");
                    }
                    return u64Value2 / u64Value1;
            }
            return 0;
        }
    }
}
