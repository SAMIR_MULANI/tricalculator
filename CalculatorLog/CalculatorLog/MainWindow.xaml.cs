﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CalculatorLog;
namespace CalculatorLog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Global Variable declaration
        long u32Number = 0;
        int u32DotCount = 0;
        int u32SlashCount = 0;
        int u32NumberCount = 0;
        int u32OperationCount = 0;
        int u32TrigOperationCount = 0;
        int FlagBit = 0;
        int OperandFlag = 0;
        int TrigFlag = 0;
        int s32AddCount = 0;
        int EligalARIgFlag = 0;
        int EligalTriARIgFlag = 0;
        float u32FloatPayload = 0;
        long u32Payload = 0;
        double u64Result = 0;
        double Result = 0;
        double u64SecondNumber = 0;
        double un32CopyNumber;
        string strCopyNumber = "";
        string strPayloadData = "";
        string strArmOperation = "";
        string strCopyPayloadData = "0";
        string strAriOperation = "";
        string strDataStore = "";
        string strOpearation = "";
        string data ="";
        string strPayloadSave = "";

        List<string> listTriFunctions = new List<string>();
        List<string> listArithmaticOperation = new List<string>();
        CalculatorLog.clsCalculationEngine objCalculationEngine = new clsCalculationEngine();

        public MainWindow()
        {
            InitializeComponent();
            DisplayData.IsEnabled = false;
        }
        private void buttonClick(object sender, RoutedEventArgs e)
        {
            listTriFunctions.Add("sin");
            listTriFunctions.Add("cos");
            listTriFunctions.Add("tan");
            listTriFunctions.Add("cot");
            listTriFunctions.Add("sec");
            listTriFunctions.Add("csc");
            listArithmaticOperation.Add("+");
            listArithmaticOperation.Add("-");

            string[] results;
            u64SecondNumber = u64Result;
            strDataStore = strPayloadSave;
           
            string strButtonData = (string)(sender as Button).Content;
            string strButtonText = strButtonData;
            if(listArithmaticOperation.Contains(strButtonData))
            {
                strAriOperation = strButtonText;
            }

            if (listTriFunctions.Contains(strButtonData))
            {
                TrigFlag = 1;

                if(strPayloadData.Contains("+") || strPayloadData.Contains("-"))
                 {
                    results = strPayloadData.Split('+');
                    if(!listTriFunctions.Contains(results[0]))
                    {
                        EligalTriARIgFlag = 1;
                    }

                 }
                if(EligalTriARIgFlag != 1)
                {
                    if (strPayloadData != "")
                    {
                        TriPayloadEngine(strButtonText, strPayloadData);
                    }
                    else
                    {
                        strPayloadData = "0";
                        TriPayloadEngine(strButtonText, strPayloadData);
                    }
                }
                else
                {
                    MessageBox.Show("Please enter the valid expression");
                }
            }
            
            else if (strButtonText == "C")
            {
                if (sender is null)
                {
                    throw new ArgumentNullException(nameof(sender));
                }
                u32Number = 0;
                EligalTriARIgFlag = 0;
                u32DotCount = 0;
                u32OperationCount = 0;
                u64Result = 0;
                u32NumberCount = 0;
                EligalARIgFlag = 0;
                data = "";
                float u32FloatPayload = 0;
                long u32Payload = 0;
                un32CopyNumber = 0;
                OperandFlag = 0;
                TrigFlag = 0;
                strPayloadData = "";
               // strOperation = "";
                strCopyNumber = "";
                strDataStore = "";
                strOpearation = "";
                strPayloadSave = "";
                DisplayData.Clear();
            }
            else
            {
                if (strButtonText != "." & strButtonText != "+" & strButtonText != "-" & strButtonText != "/")
                {
                    OperandFlag = 1;
                    if (u32NumberCount < 15)
                    {
                        
                        un32CopyNumber = (u32Number * 10) + Int32.Parse(strButtonText);
                        strCopyNumber += un32CopyNumber.ToString();
                        strPayloadData +=un32CopyNumber.ToString();
                        DisplayData.Text = strPayloadData;
                        u32NumberCount = u32NumberCount + 1;
                        strCopyPayloadData = strPayloadData;
                    }
                    else
                    {
                        MessageBox.Show("Please clear the display and again enter valid range numbers");
                    }
                }
                else if (strButtonText == ".")
                {
                    if (u32DotCount == 0)
                    {
                        strPayloadData += strButtonText;
                        DisplayData.Text = strPayloadData;
                        u32DotCount += 1;
                    }
                }
                else if (listArithmaticOperation.Contains(strButtonText) & TrigFlag == 1 | OperandFlag ==1)
                {
                    u32DotCount = 0;
                    u32SlashCount = 0;
                    TrigFlag = 0;
                    OperandFlag = 0;
                    u32TrigOperationCount = 0;
                    strArmOperation = strButtonText;
                    if (FlagBit == 1)
                    {
                        data += " " + strButtonText;
                        DisplayData.Text = data;
                        s32AddCount += 1;
                        strPayloadData = "";
                        u64Result = 0;
                        u64Result = objCalculationEngine.TriArithmaticOperation(strAriOperation, u64Result, u64SecondNumber);
                    }
                    else
                    {
                        strPayloadData +=" " + strButtonText;
                        DisplayData.Text = strPayloadData;
                    }
                   
                    FlagBit = 0;
                    strPayloadSave = "";
                }
                else if(strButtonText == "/")
                {
                    if(u32SlashCount == 0)
                    {
                        strPayloadData += strButtonText;
                        DisplayData.Text = strPayloadData;
                        u32SlashCount += 1;
                    }

                }
                else if (u32DotCount == 0)
                {
                    strDataStore += strButtonText;
                    DisplayData.Text = strPayloadData;
                   u32DotCount += 1;
                }
                else
                {
                    MessageBox.Show("Please enter the valid expressiont");
                }
            }


        }
        //public int LogCalculationEngine(string strOpearation, string strCalNumber)
        public void TriPayloadEngine(string strOpearation, string strCalNumber)
        {
            string[] strSampleCalNum;

            //MessageBox.Show("TriPayloadEngine-------------------> "+ strCalNumber);

            if (strCalNumber.Contains("/"))
            {
                strSampleCalNum = strPayloadData.Split('/');
                //MessageBox.Show("strSampleCalNum---------1----------> " + strSampleCalNum[0]);
               // MessageBox.Show("strSampleCalNum-------2------------> " + strSampleCalNum[1]);

                double num1 = double.Parse(strSampleCalNum[0]);
                double num2 = double.Parse(strSampleCalNum[1]);
                double result = 0;
                if (num2 !=0)
                {
                    result = num1 / num2;
                    strCalNumber = result.ToString();
                }
                else
                {
                    MessageBox.Show("Value Error");
                }
                MessageBox.Show("result-------2------------> " + result);
                num1 = 0;
                num2 = 0;
            }
           
            if (strCalNumber.Contains(".") & strCalNumber!= "+" & strCalNumber != "-" & strCalNumber != "/")
            {
                u32FloatPayload = float.Parse(strCalNumber);
      
            }
            else if(strCalNumber != "+" & strCalNumber != "-" & strCalNumber != "/")
            {
                
                u32Payload = int.Parse(strCalNumber);
            }
            strPayloadSave = strOpearation;

           

            //if (u32OperationCount < 5)
            //{


                if (u32OperationCount < 1)
                {

                strPayloadSave += "(" + strDataStore + (strCalNumber.Contains(".") ? u32FloatPayload.ToString() : u32Payload.ToString()) + ")";
                DisplayData.Text = strPayloadSave;
                    data += " " + strPayloadSave;

                }
                else if(s32AddCount == 1)
                {
                   
                    strPayloadSave += "(" + strDataStore + (strCalNumber.Contains(".") ? u32FloatPayload.ToString() : u32Payload.ToString()) + ")";
                    data += " " + strPayloadSave;
                    DisplayData.Text = data;
                }
                else
                { 
                    strPayloadSave += "(" + strDataStore + ")";
                    DisplayData.Text = strPayloadSave;
                    data = strPayloadSave;
                }

               
                if (u32OperationCount == 0)
                {

                double u64RadVal = (strCalNumber.Contains(".") ? u32FloatPayload : u32Payload * (Math.PI)) / 180;
                u64Result = objCalculationEngine.TriCalculationEngine(strOpearation, u64RadVal);
                DisplayData.Text += "\n" + u64Result.ToString();
                }
           
                else if (u32TrigOperationCount == 0)
                {
                    double u64RadVal = (strCalNumber.Contains(".") ? u32FloatPayload : u32Payload * (Math.PI)) / 180;
                    u64Result = objCalculationEngine.TriCalculationEngine(strOpearation, u64RadVal);
                    DisplayData.Text += "\n" + u64Result.ToString();
                }
                else
                {
                    u64Result = (u64Result * (Math.PI)) / 180;
                    u64Result  = objCalculationEngine.TriCalculationEngine(strOpearation, u64Result);
                    DisplayData.Text += "\n" + u64Result.ToString();
                }
            //return 0;

            if (s32AddCount == 1)
                {
                    u64Result = objCalculationEngine.TriArithmaticOperation(strAriOperation,u64Result,u64SecondNumber);
                    DisplayData.Clear();
                    DisplayData.Text += u64Result.ToString();
                }
                u32OperationCount += 1;
                u32TrigOperationCount += 1;
                s32AddCount = 0;
                FlagBit = 1;
               u32Payload = 0;
               u32FloatPayload = 0;

            //}
            //else
            //{
            //   MessageBox.Show("please clear the log and begin the calculation");
            // }

        }
        private void EqualButton_Click(object sender, RoutedEventArgs e)
        {
            string data = strCopyPayloadData;

            double u64Ariresult = 0;
            string[] split = strCopyPayloadData.Split(new Char[] { '+', '-','/' });
            if(strCopyPayloadData.Contains("."))
            {
                foreach (string i in split)
                {
                    if (i.Contains('.'))
                    {
                        double CopyData = double.Parse(i);
                        int someInt = (int)Math.Round(CopyData);
                        string copy = someInt.ToString();
                        data = data.Replace(i, copy);
                        MessageBox.Show("data--->" + data);
                    }
                }
                u64Ariresult = objCalculationEngine.evaluate(data);
            }

            else
            {
                u64Ariresult  = objCalculationEngine.evaluate(strCopyPayloadData);
            }
            DisplayData.Text += "\n" + u64Ariresult.ToString();

            //double result = evaluate(strCopyPayloadData);
            //MessageBox.Show("Result---->" + result.ToString());

        }
    }
}
